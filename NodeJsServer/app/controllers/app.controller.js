const fs = require("fs")
const sharp = require('sharp');
const axios = require('axios')

// require('@tensorflow/tfjs-node')

const writeFilePromise = (file, data) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(file, data, 'base64', error => {
            if (error) reject(error);
            resolve("file created successfully with handcrafted Promise!");
        });
    });
};

exports.uploadImage = async (req, res) => {



    let { img, cameraId, time, predictions } = req.body;
    let base64Data = img.replace(/^data:image\/png;base64,/, "");
    let timeTemp = new Date(time)
    let path = cameraId + "_" + timeTemp.getTime() + ".png"

    let save = await writeFilePromise(path, base64Data)
        .then(result => { return true })
        .catch(error => { return false });

    if (save) {
        setTimeout(function () {
            predictions.map((item, index) => {
                let width = parseInt(item.bbox[2])// Math.abs(parseInt(item.bbox[2]))
                let height = parseInt(item.bbox[3])// Math.abs(parseInt(item.bbox[3]))
                let left = parseInt(item.bbox[0]) //Math.abs(parseInt(item.bbox[0]))
                let top = parseInt(item.bbox[1]) //Math.abs(parseInt(item.bbox[1]))

                let originalImage = path;
                let outputImage = cameraId + "_" + timeTemp.getTime() + "_" + item.class + "_" + index + "_out_.png";
                try {
                    sharp(originalImage).extract({ width: width, height: height, left: left, top: top }).toFile(outputImage)
                        .then(function (new_file_info) {
                            console.log("Image cropped and saved");
                        })
                        .catch(function (err) {
                            console.log(err);
                        });
                } catch (err) {
                    console.log(err);
                }
            })
        }, 3000);
        res.send({ success: true })

    } else {
        res.send({ success: false })

    }
};

exports.mnistImages = async (req, res) => {
    console.log(1)
    let url = 'https://storage.googleapis.com/learnjs-data/model-builder/mnist_images.png'
    let mnist_images = await axios.get(url).then(ress => {
        return ress.data
    }).catch(error => {
        return false
    })
    res.send(mnist_images)

}

exports.mnistLabelsUint8 = async (req, res) => {
    console.log(2)
    let url = 'https://storage.googleapis.com/learnjs-data/model-builder/mnist_labels_uint8'
    let mnist_labels_uint8 = await axios.get(url).then(ress => {
        return ress.data
    }).catch(error => {
        return false
    })
    res.send(mnist_labels_uint8)
}