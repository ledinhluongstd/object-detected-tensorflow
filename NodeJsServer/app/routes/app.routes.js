module.exports = (app) => {
    const appController = require('../controllers/app.controller.js');

    // Create a new student
    app.post('/upload-image', appController.uploadImage);
    app.get('/mnist_images', appController.mnistImages);
    app.get('/mnist_labels_uint8', appController.mnistLabelsUint8)
}