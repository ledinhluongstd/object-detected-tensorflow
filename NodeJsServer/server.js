const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios')
const app = express();
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');
const cors = require('cors')
const server = require("http").createServer(app);

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json({
    limit: '16mb'
}))
app.use(bodyParser.urlencoded({
    extended: false,
    limit: '16mb'
}));

mongoose.Promise = global.Promise;
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {    // console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

// define a simple route
app.get('/', (req, res) => {
    res.json({ "message": "Welcome to EasyStudents application. Take Students quickly. Organize and keep track of all your Students." });
});
require('./app/routes/app.routes.js')(app);
// post image




// using socket io
let io = require("socket.io")(server);
let socketIO
io.on("connection", function (socket) {
    socketIO = socket
});

// listen for requests
const port = process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : 3005;
server.listen(port, () => {
    console.log("Server is listening on port " + port);
});