import React from 'react';

// import './App.css';
import 'bootstrap/dist/css/bootstrap.css';

import Stream from './index_stream'
import Video from './index_video'
function App() {
    return (
        <div className="container-fluid">
            <div className="row">
                {/* <Stream /> */}
                <Video />
            </div>

        </div>
    );
}

export default App;
