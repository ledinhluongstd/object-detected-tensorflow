import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

if (!String.prototype.format) { // define format of string
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] !== 'undefined'
                ? args[number]
                : ''
                ;
        });
    };
}



ReactDOM.render(<App />, document.getElementById('root'));
