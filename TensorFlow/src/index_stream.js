import React from "react";
import ReactDOM from "react-dom";

import * as cocoSsd from "@tensorflow-models/coco-ssd";
// import "@tensorflow/tfjs";
import "./styles.css";
import axios from 'axios'
// import * as captureVideoFrame from 'capture-video-frame'
// import extractFrames  from 'ffmpeg-extract-frames'

const upload = function (data) {
    return axios.post('http://localhost:3005/upload-image', data).then(res => {
        return res.data
    }).catch(error => {
        console.log(error)
        return null
    })
}

export default class Stream extends React.Component {
    videoRef = React.createRef();
    canvasRef = React.createRef();

    constructor(props) {
        super(props)
        this.stream = null
        this.image = null
        this.predictions = []
    }
    componentDidMount() {
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            const webCamPromise = navigator.mediaDevices
                .getUserMedia({
                    audio: false,
                    video: {
                        facingMode: "user",
                        frameRate: { ideal: 24, max: 24 }
                    }
                })
                .then(async stream => {
                    window.stream = stream;
                    this.videoRef.current.srcObject = stream;
                    return new Promise((resolve, reject) => {
                        this.videoRef.current.onloadedmetadata = () => {
                            resolve()
                        };
                    });
                });
            const modelPromise = cocoSsd.load();

            Promise.all([modelPromise, webCamPromise])
                .then(values => {
                    this.detectFrame(this.videoRef.current, values[0]);
                })
                .catch(error => {
                    console.error(error);
                });
        }
    }

    detectFrame = (video, model) => {
        model.detect(video).then(predictions => {
            this.predictions = predictions
            this.renderPredictions(predictions);
            requestAnimationFrame(() => {
                // console.log(predictions.length)
                // this.captureImage()
                if (predictions.length) {
                    // this.captureImage()
                }
                this.detectFrame(video, model);
            });
        });
    };

    renderPredictions = predictions => {

        const ctx = this.canvasRef.current.getContext("2d");
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        // Font options.
        const font = "16px sans-serif";
        ctx.font = font;
        ctx.textBaseline = "top";
        predictions.forEach(prediction => {
            const x = prediction.bbox[0];
            const y = prediction.bbox[1];
            const width = prediction.bbox[2];
            const height = prediction.bbox[3];
            // Draw the bounding box.
            ctx.strokeStyle = "#00FFFF";
            ctx.lineWidth = 1;
            ctx.strokeRect(x, y, width, height);
            // Draw the label background.
            ctx.fillStyle = "#00FFFF";
            const textWidth = ctx.measureText(prediction.class).width;
            const textHeight = parseInt(font, 10); // base 10
            ctx.fillRect(x, y, textWidth + 1, textHeight + 1);
        });

        predictions.forEach(prediction => {
            const x = prediction.bbox[0];
            const y = prediction.bbox[1];
            // Draw the text last to ensure it's on top.
            ctx.fillStyle = "#000000";
            ctx.fillText(prediction.class, x, y);
        });
    };

    async captureImage() {
        var canvas = document.getElementById("c");
        // canvas.getContext("2d").drawImage(this.videoRef.current, 0, 0, 600, 500, 0, 0, 600, 500);
        canvas.getContext("2d").drawImage(this.videoRef.current, 0, 0, 600, 450, 0, 0, 600, 450);

        this.image = canvas.toDataURL("image/png");

        // console.log(this.image)
        // alert("done");
        let uploadRes = await upload({ img: this.image, cameraId: 'CAMERA1', time: new Date(), predictions: this.predictions })
        console.log(uploadRes)
    }

    render() {
        let width = 600, height = 450
        return (
            <div className="col-md-6">
                <video
                    className="size"
                    autoPlay
                    playsInline
                    muted
                    ref={this.videoRef}
                    width={width}
                    height={height}
                />
                <canvas
                    className="size"
                    ref={this.canvasRef}
                    width={width}
                    height={height}
                />
                <canvas id="c"
                    style={{
                        position: "absolute",
                        top: "50%"
                    }}
                    width={width}
                    height={height} />

                <button id="b" onClick={() => this.captureImage()} type="button" style={{ position: 'absolute' }} >Take Picture</button>

            </div>
        );
    }
}