import React from "react";
import ReactDOM from "react-dom";
import MagicDropzone from "react-magic-dropzone";

import * as cocoSsd from "@tensorflow-models/coco-ssd";
import "@tensorflow/tfjs";
import "./styles.css";
import captureVideoFrame from "capture-video-frame"
export default class Video extends React.Component {
    videoRef = React.createRef();
    canvasRef = React.createRef();

    constructor(props) {
        super(props)
        this.state = {
            model: null,
            preview: "",
            predictions: []
        };
        this.predictions = []
        this.modelPromise = null
    }

    componentDidMount() {
        // console.log(captureVideoFrame)
        cocoSsd.load().then(model => {
            this.setState({
                model: model
            });
        });
        let modelPromise = cocoSsd.load();


        Promise.all([modelPromise])
            .then(values => {
                this.modelPromise = values[0]
                this.detectFrame(this.videoRef.current, values[0]);
            })
            .catch(error => {
                console.error(error);
            });


        console.log(this.modelPromise)
    }

    detectFrame = (video, model) => {
        model.detect(video).then(predictions => {
            console.log(predictions)
            this.predictions = predictions
            this.renderPredictions(predictions);
            requestAnimationFrame(() => {
                // console.log(predictions.length)
                // this.captureImage()
                if (predictions.length) {
                    // this.captureImage()
                }
                this.detectFrame(video, model);
            });
        });
    };

    renderPredictions = predictions => {

        const ctx = this.canvasRef.current.getContext("2d");
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        // Font options.
        const font = "16px sans-serif";
        ctx.font = font;
        ctx.textBaseline = "top";
        predictions.forEach(prediction => {
            const x = prediction.bbox[0];
            const y = prediction.bbox[1];
            const width = prediction.bbox[2];
            const height = prediction.bbox[3];
            // Draw the bounding box.
            ctx.strokeStyle = "#00FFFF";
            ctx.lineWidth = 1;
            ctx.strokeRect(x, y, width, height);
            // Draw the label background.
            ctx.fillStyle = "#00FFFF";
            const textWidth = ctx.measureText(prediction.class).width;
            const textHeight = parseInt(font, 10); // base 10
            ctx.fillRect(x, y, textWidth + 1, textHeight + 1);
        });

        predictions.forEach(prediction => {
            const x = prediction.bbox[0];
            const y = prediction.bbox[1];
            // Draw the text last to ensure it's on top.
            ctx.fillStyle = "#000000";
            ctx.fillText(prediction.class, x, y);
        });
    };

    onDrop = (accepted, rejected, links) => {
        // console.log(accepted, rejected, links)

        this.setState({ preview: accepted[0].preview || links[0] });
    };

    cropToCanvas = (image, canvas, ctx) => {
        const naturalWidth = image.naturalWidth;
        const naturalHeight = image.naturalHeight;

        canvas.width = image.width;
        canvas.height = image.height;

        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        if (naturalWidth > naturalHeight) {
            ctx.drawImage(
                image,
                (naturalWidth - naturalHeight) / 2,
                0,
                naturalHeight,
                naturalHeight,
                0,
                0,
                ctx.canvas.width,
                ctx.canvas.height
            );
        } else {
            ctx.drawImage(
                image,
                0,
                (naturalHeight - naturalWidth) / 2,
                naturalWidth,
                naturalWidth,
                0,
                0,
                ctx.canvas.width,
                ctx.canvas.height
            );
        }
    };

    onImageChange = e => {
        const c = document.getElementById("canvas");
        const ctx = c.getContext("2d");
        this.cropToCanvas(e.target, c, ctx);
        this.state.model.detect(c).then(predictions => {
            // Font options.
            const font = "16px sans-serif";
            ctx.font = font;
            ctx.textBaseline = "top";

            predictions.forEach(prediction => {
                const x = prediction.bbox[0];
                const y = prediction.bbox[1];
                const width = prediction.bbox[2];
                const height = prediction.bbox[3];
                // Draw the bounding box.
                ctx.strokeStyle = "#00FFFF";
                ctx.lineWidth = 4;
                ctx.strokeRect(x, y, width, height);
                // Draw the label background.
                ctx.fillStyle = "#00FFFF";
                const textWidth = ctx.measureText(prediction.class).width;
                const textHeight = parseInt(font, 10); // base 10
                ctx.fillRect(x, y, textWidth + 4, textHeight + 4);
            });

            predictions.forEach(prediction => {
                const x = prediction.bbox[0];
                const y = prediction.bbox[1];
                // Draw the text last to ensure it's on top.
                ctx.fillStyle = "#000000";
                ctx.fillText(prediction.class, x, y);
            });
        });
    };

    capture() {
        var frame = captureVideoFrame('my-video', 'png');
        // Show the image

        console.log(frame)
        var img = document.getElementById('my-screenshot');
        img.setAttribute('src', frame.dataUri);
    }
    render() {
        let width = 600, height = 450

        return (
            <div className="Dropzone-page col-md-6">
                {this.state.model ? (
                    <MagicDropzone
                        className="Dropzone"
                        accept="image/jpeg, image/png, .jpg, .jpeg, .png"
                        multiple={false}
                        onDrop={this.onDrop}
                    >
                        {this.state.preview ? (
                            <img
                                alt="upload preview"
                                onLoad={this.onImageChange}
                                className="Dropzone-img"
                                src={this.state.preview}
                            />
                        ) : (
                                "Choose or drop a file."
                            )}
                        <canvas id="canvas" />
                    </MagicDropzone>
                ) : (
                        <div className="Dropzone">Loading model...</div>
                    )}

                <video
                    id="my-video"
                    controls
                    autoPlay={true}
                    width={width}
                    height={height}
                    ref={this.videoRef}
                    src={"input_video.mp4"}
                    // style={{ position: 'absolute' }}
                >
                </video>
                <img id="my-screenshot" width={width}
                    height={height} />
                <canvas
                    className="size-video"
                    ref={this.canvasRef}
                    width={width}
                    height={height}
                    // style={{ position: 'absolute' }}

                />
                <button onClick={() => this.capture()}>Cắt ảnh</button>
            </div>
        );
    }
}